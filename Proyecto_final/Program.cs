﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_final
{
    class Program:nodo
    {
        static void Main(string[] args)
        {
            Console.WriteLine("=============================================================================================");
            TextReader Leer_archivo;
            Leer_archivo = new StreamReader("archivo.txt");
            Console.WriteLine(Leer_archivo.ReadToEnd());
            Leer_archivo.Close();
            Console.WriteLine("=============================================================================================");

            Program l = new Program();
            l.insertarnodo();
            l.insertarnodo();
            l.insertarnodo();
            l.insertarnodo();


            Console.WriteLine("\n\n La lista de pedidos es: ");

            l.leerLista();
            /*/Console.WriteLine("=============================================================");
            TextWriter archivo;
            archivo = new StreamWriter("archivo.txt");
            string mensaje;
            mensaje = Console.ReadLine();
            archivo.WriteLine(mensaje);
            archivo.Close();
            Console.Clear();
            Console.WriteLine("=============================================================");*/
            


            TextWriter TextoPlano;
            TextoPlano = new StreamWriter("textoPlano.txt");
            string mensaje;
            mensaje = Console.ReadLine();
            TextoPlano.WriteLine(mensaje);
            TextoPlano.Close();
            


            /*TextReader mostrar_texto;
            mostrar_texto = new StreamReader("TextoPlano.txt");
            string sacar = mostrar_texto.ReadToEnd();
            mostrar_texto.Close();
            Console.WriteLine("" + sacar);
            TextWriter archivo;
            archivo = new StreamWriter("archivo.txt");
            string aviso;
            aviso = Console.ReadLine();
            archivo.WriteLine(aviso);
            archivo.Close();
            Console.Clear();


            TextReader llamar_texto;
            llamar_texto = new StreamReader("TextoPlano.txt");
            string jalar = llamar_texto.ReadToEnd();
            llamar_texto.Close();
            Console.WriteLine("" + jalar);*/    
           
        }
        private nodo primero = new nodo();
        private nodo ultimo = new nodo();
        public Program()
        {
            primero = null;
            ultimo = null;
        }

        public void insertarnodo()
        {
            nodo nuevo = new nodo();
            Console.WriteLine("ingrese el dato para el nodo:");
            nuevo.Dato = int.Parse(Console.ReadLine());

            if(primero == null)
            {
                primero = nuevo;
                primero.Siguiente = null;
                ultimo = nuevo;
            }
            else
            {
                ultimo.Siguiente = nuevo;
                nuevo.Siguiente = null;
                ultimo = nuevo;
            }
            Console.WriteLine("\n Nodo nuevo \n");
        }
        

        public void leerLista()
        {
            nodo actual = new nodo();
            actual = primero;

            if (primero != null)
            {
                while (actual != null)
                {
                    Console.WriteLine(" {0} ", actual.Dato);
                    actual = actual.Siguiente;
                }
            }
            else
            {
                Console.WriteLine("\n la lista esta vacia \n");
            }
            
        }
    }
}
