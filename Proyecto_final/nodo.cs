﻿using System;


namespace Proyecto_final
{
    class nodo
    {
        private int dato;
        private nodo siguiente;


        public int Dato
        {
            get { return dato; }
            set { dato = value; }
        }
        public nodo Siguiente
        {
            get { return siguiente; }
            set { siguiente = value; }
        }
    }
}
